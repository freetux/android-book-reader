# Book Reader

Book Reader simple UI. Based on FBReader (https://github.com/geometer/FBReaderJ):

Supported formats: fb2, html, txt, epub, mobi, rtf, doc, pdf, djvu.

All the best in this world is free!

# Manual install

    gradle installDebug

# Translate

If you want to translate 'Book Reader' to your language  please read following:

  * [HOWTO-Translate.md](/docs/HOWTO-Translate.md)

# Screenshots

![shot](/docs/shot.png)

# Contributors

  * nepali translation thanks to @bkb1